/*
  ==============================================================================

    GUITools.h
    Created: 21 Mar 2019 9:06:44am
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#ifndef __DSTEAM_GUI_TOOLS__
#define __DSTEAM_GUI_TOOLS__

namespace dsteam
{
    namespace gui
    {
        void scaleLabelFont(Label& label);

        void fillAll(Graphics& g, Component* component);

        class Dial : public Slider
        {
        public:
            Dial()
            {
                setSliderStyle(Slider::RotaryHorizontalVerticalDrag);
                setTextBoxStyle(Slider::NoTextBox, false, 90, 15);
                setPopupDisplayEnabled(true, false, this);
            }
        };

        // ----------------------------------------------------------------------------

        template <class ControllerType, class AttachmentClass>
        class ControllerWithAttachment : public ControllerType
        {
        public:
            ControllerWithAttachment()
            {}

            ControllerWithAttachment(AudioProcessorValueTreeState& apvts, const String& paramId)
            {
                init(apvts, paramId);
            }

            void init(AudioProcessorValueTreeState& apvts, const String& paramId)
            {
                attachment.reset(new AttachmentClass(apvts, paramId, *this));

                auto param = apvts.getParameter(paramId);
                ControllerType::setName(param->name);
            }

            std::unique_ptr<AttachmentClass> attachment;
        };

        template <>
        class ControllerWithAttachment
            <ComboBox, AudioProcessorValueTreeState::ComboBoxAttachment>
            : public ComboBox
        {
        public:
            ControllerWithAttachment(AudioProcessorValueTreeState& apvts, const String& paramId)
            {
                attachment.reset(new AudioProcessorValueTreeState::ComboBoxAttachment(apvts, paramId, *this));

                const AudioParameterChoice* param = dynamic_cast<AudioParameterChoice*>(apvts.getParameter(paramId));
                setName(param->name);

                int i = 1;
                for (auto choice : param->choices)
                {
                    addItem(choice, i++);
                }
            }

            std::unique_ptr<AudioProcessorValueTreeState::ComboBoxAttachment> attachment;
        };

        typedef ControllerWithAttachment<Dial, AudioProcessorValueTreeState::SliderAttachment> DialWithAttachment;
        typedef ControllerWithAttachment<ToggleButton, AudioProcessorValueTreeState::ButtonAttachment> ToggleWithAttachment;
        typedef ControllerWithAttachment<Slider, AudioProcessorValueTreeState::SliderAttachment> SliderWithAttachment;
        typedef ControllerWithAttachment<ComboBox, AudioProcessorValueTreeState::ComboBoxAttachment> ComboBoxWithAttachment;

        // ----------------------------------------------------------------------------

        template <class T>
        Rectangle<T> getMaxInnerSquare(Rectangle<T> componentArea)
        {
            T diameter = jmin(componentArea.getWidth(), componentArea.getHeight());
            return
            {
                static_cast<T>
                (
                    componentArea.getX() + 0.5f * (componentArea.getWidth() - diameter)
                ),
                componentArea.getY(),
                diameter,
                diameter
            };
        }

        // -----------------------------------------------------------------------

        inline Rectangle<float> intToFloatRectangle(Rectangle<int> irect)
        {
            return irect.toFloat();
        }

        // ----------------------------------------------------------------------------
    } // namespace gui
} // namespace dsteam

#endif // __DSTEAM_GUI_TOOLS__
