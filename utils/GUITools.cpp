#include "GUITools.h"

namespace dsteam
{
    namespace gui
    {
        void scaleLabelFont(Label& label)
        {
            float textWidth = label.getFont().getStringWidthFloat(label.getText());
            float textSizeRatio = label.getFont().getHeight() / textWidth;
            float newTextHeight = textSizeRatio * label.getWidth();
            newTextHeight = newTextHeight < label.getHeight() ? newTextHeight : label.getHeight();
            label.setFont(newTextHeight);
        }

        void fillAll(Graphics& g, Component* component)
        {
            ignoreUnused(component);
            //juce::Random rand{ Time::currentTimeMillis() };
            //g.fillAll(Colour(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), 1.f));
            g.fillAll(component->getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
        }
    }
}
