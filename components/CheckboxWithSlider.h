/*
  ==============================================================================

    CheckboxWithSlider.h
    Created: 10 Jun 2019 12:35:03pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

namespace dsteam
{
    namespace gui
    {
    //==============================================================================
    /*
    */
    class CheckboxWithSlider    : public Component
    {
    public:
        CheckboxWithSlider()
        {
            setOpaque(true);
        }

        void init(
            AudioProcessorValueTreeState& apvts, const String& checkboxId, const String& sliderId,
            String preLabelText, String checkboxText, String labelText, Range<double> sliderRange, double sliderStep,
            Font font
        )
        {
            preLabel.setText(preLabelText, NotificationType::dontSendNotification);
            preLabel.setFont(font);
            addAndMakeVisible(preLabel);

            trackCheckbox.init(apvts, checkboxId);
            trackSlider.init(apvts, sliderId);

            addAndMakeVisible(trackCheckbox);
            trackCheckbox.setButtonText(checkboxText);

            trackSlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
            trackSlider.setIncDecButtonsMode(Slider::IncDecButtonMode::incDecButtonsDraggable_AutoDirection);
            trackSlider.setRange(sliderRange, sliderStep); // Beacuse of bug in JUCE
            trackSlider.setTextBoxStyle(Slider::TextBoxLeft, false, 25, 20);
            addAndMakeVisible(trackSlider);

            trackSliderLabel.setText(labelText, NotificationType::dontSendNotification);
            addAndMakeVisible(trackSliderLabel);
        }

        ~CheckboxWithSlider() override
        {
        }

        void paint(Graphics& g) override
        {
            dsteam::gui::fillAll(g, this);
        }

        void resized() override
        {
            Rectangle<int> area = getLocalBounds();
            preLabel.setBounds(area.removeFromLeft(20));
            trackCheckbox.setBounds(area.removeFromLeft(120));
            trackSlider.setBounds(area.removeFromLeft(65));//.reduced(0, 8));
            trackSliderLabel.setBounds(area);// .reduced(0, 8));
        }

    private:
        Label preLabel;
        dsteam::gui::ToggleWithAttachment trackCheckbox;
        dsteam::gui::SliderWithAttachment trackSlider;
        Label trackSliderLabel;

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (CheckboxWithSlider)
    };
    }
}
