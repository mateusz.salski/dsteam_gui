/** BEGIN_JUCE_MODULE_DECLARATION

    ID:            dsteam_gui
    vendor:         Mateusz Salski
    version:        0.0.1
    name:          Digital Steam - Simple and common GUI functions and classes.
    description:     A set of function and classes coded while learning JUCE and DSP.
    website:        https://digitalsteam.pl
    license:        ISC

    dependencies:   juce_gui_basics

    END_JUCE_MODULE_DECLARATION
*/

#pragma once
#define JUCE_AUDIO_PROCESSORS_H_INCLUDED

#ifndef __DSTEAM_GUI_H_INCLUDED
#define __DSTEAM_GUI_H_INCLUDED

#include <juce_core/juce_core.h>
#include <juce_audio_basics/juce_audio_basics.h>
#include <juce_audio_processors/juce_audio_processors.h>
#include <juce_dsp/juce_dsp.h>

#if ! DONT_SET_USING_JUCE_NAMESPACE
// If your code uses a lot of JUCE classes, then this will obviously save you
// a lot of typing, but can be disabled by setting DONT_SET_USING_JUCE_NAMESPACE.
using namespace juce;
#endif

#include "utils/GUITools.h"
#include "components/CheckboxWithSlider.h"

#endif
